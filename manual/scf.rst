.. _scf-manual:

SCF – ground state wave function optimization
=============================================

Run SCF job
-----------

The command to run unperturbed SCF looks as follows,
::

  $ RUN_RS-mDKS [-np number-of-processors] [-scratch /path/to/scratch] name-of-scf-input(.inp)

where
::

  RUN_RS-mDKS                 is a script for running ReSpect SCF
  name-of-scf-input.inp       is a input file to ReSpect SCF
  [-np number-of-processors]  an optional argument specifying the number of MPI tasks
  [-scratch /path/to/scratch] an optional argument specifying the path to scratch directory

Notes:

  * The *RUN_RS-mDKS* script is located in the main ReSpect directory
  * The presence of the input file *name-of-scf-input.inp* is mandatory
  * The output file will be named *name-of-scf-input.out*
  * Optionally, one can create a restart file *name-of-scf-input.rest* (see
    INITMO RESTART keyword). The restart file is a copy of .50 file obtained
    from a previous job; cp *name-of-previous-scf-input.50*
    *name-of-scf-input.rest*)


SCF input file
--------------

Table of keywords
~~~~~~~~~~~~~~~~~

  +--------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------+
  | Short description              | Keyword           | Characteristic                                                                                               |
  +--------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------+
  | :ref:`scf-title`               | TITLE             | Specify the title of your job                                                                                |
  +--------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------+
  | :ref:`scf-geometry`            | ZMATRIX/CARTESIAN | Specify the molecular geometry                                                                               |
  +--------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------+
  | :ref:`scf-hamiltonian`         | METHOD            | Specify the Hamiltonian                                                                                      |
  +--------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------+
  | :ref:`scf-initial-guess`       | INITMO            | Specify the initial guess for molecular orbitals                                                             |
  +--------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------+
  | :ref:`scf-dft-potentials`      | POTENTIAL         | Specify the DFT XC potential/functional                                                                      |
  +--------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------+
  | :ref:`scf-dft-grid`            | GRID              | Specify the DFT grid (global to all atoms)                                                                   |
  +--------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------+
  | :ref:`scf-charge-distribution` | NUCLEUS           | Specify the nuclear charge distribution model                                                                |
  +--------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------+
  | :ref:`scf-charge`              | CHARGE            | Specify the charge of the system                                                                             |
  +--------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------+
  | :ref:`scf-multiplicity`        | MULTIPLICITY      | Specify the multiplicity of the system                                                                       |
  +--------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------+
  | :ref:`scf-maximum-iterations`  | MAXITERATIONS     | Specify the maximum number of SCF iterations                                                                 |
  +--------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------+
  | :ref:`scf-convergence`         | CONVERGENCE       | Specify the SCF convergence criteria                                                                         |
  +--------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------+
  | :ref:`scf-spin`                | SPIN              | Specify the spin quantization axis (the keyword is mandatory for four-component EPR calculations)            |
  +--------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------+
  | :ref:`scf-metric`              | METRIC            | Specify the metric used in fitting of densities                                                              |
  +--------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------+
  | :ref:`scf-diis`                | DIIS              | Specify the DIIS parameters (Direct Inversion in the Iterative Subspace)                                     |
  +--------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------+
  | :ref:`scf-cvalue`              | CVALUE            | Specify own value for the speed of light (in atomic units)                                                   |
  +--------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------+
  | :ref:`scf-cscale`              | CSCALE            | Specify the scaling factor for the speed of light (scale all relativistic corrections)                       |
  +--------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------+
  | :ref:`scf-soscale`             | SOSCALE           | Specify the scaling factor for the spin-orbit interaction (scale only the spin-orbit interaction)            |
  +--------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------+
  | :ref:`scf-checkpoint`          | CHKPOINT          | Creating a checkpoint \*.50 files during SCF cycles                                                          |
  +--------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------+
  | :ref:`scf-eri-class`           | ERI_CLASS         | Specify the final class of electron repulsion integrals used for the four-component Fock matrix construction |
  +--------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------+
  | :ref:`scf-basis`               | SUBSTITUTE        | Basis and DFT grid specification (atom-by-atom)                                                              |
  +--------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------+

.. _scf-title:

Title
~~~~~
::

  --- input block ---
  [TITLE]
  <string>
  --- input block ---

  <string>  -->  string of maximum 80 characters

::

  --- example ---
  TITLE
  CH4; geometry from JCP XXXXX
  --- example ---

.. _scf-geometry:

Geometry
~~~~~~~~
::

  --- input block ---
  [key] <opt1>
  <atom1> ...
  <atom2> ...
  ...
  [END]
  --- input block ---

  specify the geometry format:
  [key]   -->  ZMATRIX    -->  Z-matrix format
          -->  CARTESIAN  -->  XYZ format

  specify the units:
  <opt1>  -->  ANGSTROM
          -->  BOHR (default)

  specify the atoms:
  <atomx> -->  label of atom; first letter must be always capital (e.g. He or HE)

::

  --- example 1 ---
  ZMATRIX ANGSTROM
   C
   H    1   1.09000
   H    1   1.09000   2   109.47
   H    1   1.09000   2   109.47     3    120.00
   H    1   1.09000   2   109.47     3   -120.00
  END
  --- example 1 ---

::

  --- example 2 ---
  CARTESIAN BOHR
   C    0.000000    0.000000    0.000000
   H    0.000000    0.000000    2.059802
   H    1.942014    0.000000   -0.686559
   H   -0.971007    1.681834   -0.686559
   H   -0.971007   -1.681834   -0.686559
  END
  --- example 2 ---

.. _scf-hamiltonian:

Hamiltonian
~~~~~~~~~~~
::

  --- input block ---
  [METHOD] <opt>
  --- input block ---

  <opt>  -->  KS          one-component  Kohn-Sham    apprach
         -->  HF          one-component  Hartree-Fock approach
         -->  KS-DKH2     one-component  Kohn-Sham    approach including scalar second-order Douglas-Kroll-Hess one-electron relativistic corrections
         -->  HF-DKH2     one-component  Hartree-Fock approach including scalar second-order Douglas-Kroll-Hess one-electron relativistic corrections
         -->  mDKS        four-component Kohn-Sham    approach
         -->  mDHF        four-component Hartree-Fock approach
         -->  mDKS-RI     four-component Kohn-Sham    approach using the resolution-of-identity (RI) technique for fast, but approximate, evaluation of
                                                      both Coulomb and DFT exchange term (note: available only for non-hybrid DFT functionals)

::

  --- default ---
  METHOD mDKS-RI
  --- default ---

::

  --- example ---
  METHOD KS-DKH2
  --- example ---

.. _scf-initial-guess:

Initial guess
~~~~~~~~~~~~~
::

  --- input block ---
  [INITMO] <opt>
  --- input block ---

  <opt>  -->  BARE     -->  bare nucleus
         -->  EHT      -->  Extended Hueckel Theory
         -->  ATOMIC   -->  superposition of atomic densities
         -->  RESTART  -->  initial MOs are taken from the restart file: name-of-scf-job.rest
                            (note, the restart file must already exist in the directory; usually it is copy of *.50 file from a previous job)

::

  --- default ---
  INITMO ATOMIC
  --- default ---

::

  --- example ---
  INITMO RESTART
  --- example ---

.. _scf-dft-potentials:

DFT potentials
~~~~~~~~~~~~~~
::

  --- input block ---
  [POTENTIAL] <opt>
  --- input block ---

  <opt>  -->  SLATER  -->          Slater exchange
         -->  LOCAL   -->  LDA:    Slater exchange                 /  VWN(5) correlation
         -->  BP86    -->  GGA:    Becke exchange                  /  Perdew correlation
         -->  PW91    -->  GGA:    Perdew exchange                 /  Perdew correlation
         -->  PBE     -->  GGA:    Perdew-Burke-Ernzerhof exchange /  Perdew-Burke-Ernzerhof correlation
         -->  BLYP    -->  GGA:    Becke exchange                  /  Lee-Yang-Parr correlation
         -->  B3LYP   -->  HYBRID: Becke 3 parameters exchange     /  Lee-Yang-Parr correlation
         -->  PBE0    -->  HYBRID: Perdew-Burke-Ernzerhof exchange /  Perdew-Burke-Ernzerhof correlation

::

  --- default ---
  POTENTIAL LOCAL
  --- default ---

::

  --- example ---
  POTENTIAL B3LYP
  --- example ---

*Update*

Since the version 3.3.0 (2013), ReSpect allows to use hybrid functionals for SCF and
EPR property calculations. These functionals include a mixture of (Dirac-)
Hartree-Fock exchange (HFX) with DFT exchange-correlation. Among the
functionals, the most popular is the Becke's three parameter hybrid functional
(B3) combined with the non-local correlation provided by the LYP expression
(LYPC), and VWN5 functional for local correlation (VWN5C), commonly known as
B3LYP. It has the following form: a*HFX + (1-a)*SlaterX + b*BeckeX + c*LYPC +
(1-c)*VWN5C, with default parameters a=0.20, b=0.72, c=0.81. The ReSpect allows
to vary the amount of HFX (a-parameter) from the input, for instance 30% HFX
can be changed by

::

  --- example ---
  POTENTIAL B3LYP 0.3
  --- example ---

.. _scf-dft-grid:

DFT grid
~~~~~~~~

A common grid for all atoms. To set an atom specific grid, see keyword SUBSTITUTE

::

  --- input block ---
  [GRID] <opt1> |and| <opt2>
  --- input block ---


  <opt1>  -->  positive integer number specifying the number of radial grid points
               (we recommend to use a rule <opt1>=40+10*N, where N is the row in periodic table of the heaviest element in a molecule)

  <opt2>  -->  positive integer specifying the number of Lebedev angular points per one radial point. The accepted values are
          -->  110
          -->  146
          -->  194
          -->  302
          -->  434
          -->  590
          -->  770
          -->  974
          -->  1202
          -->  ADAPTIVE (recommended)

::

  --- default ---
  GRID 100 302
  --- default ---

::

  --- example ---
  GRID 60 ADAPTIVE
  --- example ---

.. _scf-charge-distribution:

Nuclear charge distribution model
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
::

  --- input block ---
  [NUCLEUS] <opt>
  --- input block ---

  <opt>  -->  POINT  -->  Point nucleus
         -->  GAUSS  -->  Finite (Gauss) nucleus

::

  --- default ---
  NUCLEUS POINT
  --- default ---

::

  --- example ---
  NUCLEUS GAUSS
  --- example ---

.. _scf-charge:

Charge of the system
~~~~~~~~~~~~~~~~~~~~
::

  --- input block ---
  [CHARGE] <opt>
  --- input block ---

  <opt>  -->  natural number specifying the charge of a system

::

  --- default ---
  CHARGE 0
  --- default ---

::

  --- example ---
  CHARGE -1
  --- example ---

.. _scf-multiplicity:

Multiplicity of the system
~~~~~~~~~~~~~~~~~~~~~~~~~~
::

  --- input block ---
  [MULTIPLICITY] <opt>
  --- input block ---

  <opt>  -->  non-negative integer specifying the multiplicity of a system

::

  --- default ---
  MULTIPLICITY 1
  --- default ---

::

  --- example ---
  MULTIPLICITY 2
  --- example ---

.. _scf-maximum-iterations:

Maximum number of SCF iterations
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
::

  --- input block ---
  [MAXITERATIONS] <opt>
  --- input block ---

  <opt>  -->  positive integer number specifying the maximum number of SCF iterations

::

  --- default ---
  MAXITER 50
  --- default ---

::

  --- example ---
  MAXITER 30
  --- example ---

.. _scf-convergence:

SCF convergence criteria
~~~~~~~~~~~~~~~~~~~~~~~~
::

  --- input block ---
  [CONVERGENCE] <opt>
  --- input block ---

  <opt>  -->  positive real number specifying the SCF convergence criteria in Hartree units

::

  --- default ---
  CONVERGENCE 1.0D-5
  --- default ---

::

  --- example ---
  CONVERGENCE 0.0000001
  --- example ---

.. _scf-diis:

DIIS
~~~~

DIIS - Direct Inversion in the Iterative Subspace (SCF convergence acceleration tool)

::

  --- input block ---
  [DIIS] <opt1> |I| { <opt2> |and| <opt3> |and| <opt4> }
  --- input block ---

  <opt1>  -->  OFF  -->  DIIS scheme is turned off
          -->  ON   -->  DIIS scheme is turned on

  <opt2>  -->  positive integer number specifying the first Fock matrix for DIIS space buildup

  <opt3>  -->  positive integer number specifying the minimum dimension of the DIIS space

  <opt4>  -->  positive integer number specifying the maximun dimension of the DIIS space

::

  --- default ---
  DIIS ON 2 4 49
  --- default ---

::

  --- example ---
  DIIS OFF
  --- example ---

.. _scf-spin:

Spin quantization axis
~~~~~~~~~~~~~~~~~~~~~~
::

  --- input block ---
  [SPIN] <opt1>
  --- input block ---

  Specify the spin quantization (note, the keyword has meaning only in the combination with INITMO EHT/ATOMIC/RESTART and four-component EPR calculations)
  <opt1>  -->  X  -->  the quantization axis along X direction
          -->  Y  -->  the quantization axis along Y direction
          -->  Z  -->  the quantization axis along Z direction

::

  --- default ---
  SPIN Z
  --- default ---

::

  --- example ---
  SPIN X
  --- example ---

.. _scf-metric:

Metric used in fitting of densities
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
::

  --- input block ---
  [METRIC] <opt>
  --- input block ---

  <opt>  -->  COULOMB
         -->  OVERLAP

::

  --- default ---
  METRIC COULOMB
  --- default ---

::

  --- example ---
  METRIC OVERLAP
  --- example ---

.. _scf-cvalue:

Define the speed of light
~~~~~~~~~~~~~~~~~~~~~~~~~
::

  --- input block ---
  [CVALUE] <opt>
  --- input block ---

  <opt>  -->  real number specifying a new speed of light value

::

  --- default ---
  CVALUE 137.035999074d0
  --- default ---

::

  --- example ---
  CVALUE 1370.35999074d0    (make a system more "non-relativistic" by increasing the speed of light value 10x)
  --- example ---

.. _scf-cscale:

Scale the speed of light
~~~~~~~~~~~~~~~~~~~~~~~~
::

  --- input block ---
  [CSCALE] <opt>
  --- input block ---

  <opt>  -->  real number specifying a scaling factor for the speed of light value

::

  --- default ---
  CSCALE 1.0d0
  --- default ---

::

  --- example ---
  CSCALE 50.0d0    (make a system more "non-relativistic" by increasing the speed of light value 50x)
  --- example ---

.. _scf-soscale:

Scale the spin-orbit interaction
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
::

  --- input block ---
  [SOSCALE] <opt>
  --- input block ---

  <opt>  -->  real number specifying a scaling factor in front of spin-orbit (SO) interaction terms.
              One can examine the dependence of total energy on the SO interaction.

::

  --- default ---
  SOSCALE 1.0d0
  --- default ---

::

  --- example ---
  SOSCALE 0.0d0    (it makes the Dirac-Kohn-Sham and Dirac-Hartree-Fock calculations running in the spin-orbit-free (SOF) mode, i.e. without spin-orbit interaction)
  --- example ---

.. _scf-checkpoint:

Checkpoint files
~~~~~~~~~~~~~~~~
::

  --- input block ---
  [CHKPOINT] <opt>
  --- input block ---

  <opt>  -->  positive integer number specifying the frequency of creating a checkpoint (.50) file

::

  --- default ---
  CHKPOINT 10
  --- default ---

::

  --- example ---
  CHKPOINT 5
  --- example ---

.. _scf-eri-class:

Electron repulsion integral class specification
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In the four-component calculations based on the Dirac-Coulomb Hamiltonian, we
have three classes of two-electron repulsion integrals (ERI), namely [LL|LL],
[SS|LL] and [SS|SS], where L/S refers to the large/small component basis set of
any four AO indexes. The computational complexity for evaluating those integral
classes grows in the order [LL|LL] < [SS|LL] < [SS|SS], whereas their relative
contribution to the Fock matrix has a reverse order. One can therefore specify
certain ERI class as a upper limit beyond which all two-electron contributions
are going to be neglected. This option/keyword can speed-up calculations
significantly.  **Be careful** when using the most aggressive eri-cutoffs,
namely the "ERI_CLASS SSLL AABB" option.

::

  --- input block ---
  [ERI_CLASS] <opt>
  --- input block ---

  <opt>  -->  SSLL AABB   all [LL|LL] and only those [SS|LL] & [SS|SS] ERIs which have the SS-pair (both bra and ket) on the same atomic center (severe approximation, check results!)
         -->  SSSS AABB   all [LL|LL] & [SS|LL] and only those [SS|SS] ERIs which have the SS-pair (both bra and ket) on the same atomic center (recommended approximation)
         -->  SSSS ABCD   all [LL|LL] & [SS|LL] & [SSSS] (exact approach)

::

  --- default ---
  ERI_CLASS SSSS AABB
  --- default ---

::

  --- example ---
  ERI_CLASS SSSS ABCD
  --- example ---

.. _scf-basis:

Basis and grid specification
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
::

  --- input block ---
  [SUBSITUTE] <opt1>
  <AO   string> |or|
  <AUX  string> |or|
  <GRID string>
  --- input block ---


  <opt1>  -->  position    -->  define basis sets and DFT grid for a atom being at a given "position" on the molecule list in the GEOMETRY section
          -->  atom-label  -->  define basis sets and DFT grid for all atoms having the "atom-label", for instance all Br atoms in the molecule


  Define an atomic orbital basis:
  <AO string>   --> AO  name_of_the_orbital_basis   --> The name of an AO basis (note: length of the basis name should be at most 50 characters)

  Define an auxialiary basis:
  <AUX string>  --> AUX name_of_the_auxiliary_basis --> The name of an auxiliary basis used for the RI approach (note: length of the basis name should be at most 50 characters)

  Define an atom specific grid:
  <GRID string>  -->  GRID <opt1> |and| <opt2>  --> <opt1> and <opt2> are the same as for GRID keyword

::

  --- default ---
  There is no default yet. The basis must be specified for every atom in this section!
  --- default ---

::

  --- example 1: substitution of AO basis, AUX basis and GRID for all hydrogens ---
  SUBSTITUTE H
  AO   O-H  ucc-pvdz
  AUX  A-H  ucc-pvdz
  GRID 50   ADAPTIVE
  --- example 1 ---

::

  --- example 2: substitute of AO basis and AUX basis for the first atom (the atom labeling matches the GEOMETRY) ---
  SUBSTITUTE 1
  AO  O-C  upc-2
  AUX A-C  upc-2
  --- example 2 ---
