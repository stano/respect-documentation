.. _mag-manual:

MAG – magnetic properties module
================================

Calculate magnetic properties
-----------------------------

The command to run MAG module of the ReSpect program looks as follows

::

  $ MAG-script [-np number_of_processors] [-scratch /path/to/scratch] name-of-the-job name-of-the-MAG-input

where

::

  MAG-script=RUN_MAG-mDKS_EPR         is the script for calculation of g-tensor or hyperfine tensor
  MAG-script=RUN_MAG-mDKS_NMR         is the script for calculation of NMR shielding tensor, spin-spin coupling tensor
                                      or nuclear spin-rotation coupling tensor
  name-of-the-job.inp                 is the name of your input file for SCF calculations
  name-of-the-MAG-input.M             is the name of your input for MAG part of ReSpect
  -np number_of_processors            an optional argument specifying number of cores in MPI calculation
  -scratch /path/to/scratch           an optional argument specifying the path to scratch directory


Notes:

  * MAG scripts (RUN_MAG-mDKS_EPR and RUN_MAG-mDKS_NMR) are present in your
    install directory.
  * Mandatory files are: information about unperturbed SCF *name-of-the-job.50*
    and input file for MAG *name-of-the-MAG_input.M*
  * name-of-the-job.11 file contains information about grid. This file is
    necessary in NMR calculations. You can omit it if you will specify grid in
    MAG input (.M file).
  * The output file will have the name: *name-of-the-job-name-of-the-MAG-input.OUT*

MAG input file
--------------

Table of Keywords
~~~~~~~~~~~~~~~~~

  +---------------------------------+--------------+-------------------------------------------+
  | Short description               | Keyword      | Characteristic                            |
  +---------------------------------+--------------+-------------------------------------------+
  | :ref:`mag-property`             | KEYMAG       | Property                                  |
  +---------------------------------+--------------+-------------------------------------------+
  | :ref:`mag-substitute`           | SUBSTITUTE   | Specify properties of atoms               |
  +---------------------------------+--------------+-------------------------------------------+
  | :ref:`mag-reference-atom`       | REFATOM      | Reference atom                            |
  +---------------------------------+--------------+-------------------------------------------+
  | :ref:`mag-nmr-atoms`            | NMRATOM      | NMR atoms                                 |
  +---------------------------------+--------------+-------------------------------------------+
  | :ref:`mag-common-guage-origin`  | COMMONGAUGE  | Common guage origin                       |
  +---------------------------------+--------------+-------------------------------------------+
  | :ref:`mag-level-of-theory`      | PERTURBATION | Level of theory                           |
  +---------------------------------+--------------+-------------------------------------------+
  | :ref:`mag-operator`             | OPERATOR     | Operator                                  |
  +---------------------------------+--------------+-------------------------------------------+
  | :ref:`mag-gauge-origin-problem` | METHOD       | Method to solve gauge origin problem      |
  +---------------------------------+--------------+-------------------------------------------+
  | :ref:`mag-kernel`               | KERNEL       | Method for calculation of kernel          |
  +---------------------------------+--------------+-------------------------------------------+
  | :ref:`mag-type-of-kernel`       | POTENTIAL    | Type of kernel                            |
  +---------------------------------+--------------+-------------------------------------------+
  | :ref:`mag-diis`                 | DIIS         | Direct Inversion in the Iterative Space   |
  +---------------------------------+--------------+-------------------------------------------+
  | :ref:`mag-mixing-parameter`     | DMIXING      | Mixing parameter                          |
  +---------------------------------+--------------+-------------------------------------------+
  | :ref:`mag-maximum-iterations`   | MAXITER      | Maximum number of SCF iterations          |
  +---------------------------------+--------------+-------------------------------------------+
  | :ref:`mag-convergent-criteria`  | CONVERGENCE  | Convergent criteria                       |
  +---------------------------------+--------------+-------------------------------------------+
  | :ref:`mag-cscale`               | CSCALE       | Scale speed of light                      |
  +---------------------------------+--------------+-------------------------------------------+
  | :ref:`mag-output-file`          | OUTPUT       | Form of output file(s)                    |
  +---------------------------------+--------------+-------------------------------------------+
  | :ref:`mag-charge-distribution`  | NUCLEUS      | Charge distribution of nucleus            |
  +---------------------------------+--------------+-------------------------------------------+
  | :ref:`mag-magnetic-moment`      | MAGMOMENT    | Type of magnetic moment of nucleus        |
  +---------------------------------+--------------+-------------------------------------------+
  | :ref:`mag-density2grid`         | R2GRID       | Calculate density on the grid (CUBE file) |
  +---------------------------------+--------------+-------------------------------------------+
  | :ref:`mag-keys-for-R2GR`        | KEYS         | Options for R2GRID keyword                |
  +---------------------------------+--------------+-------------------------------------------+
  | :ref:`mag-current-density`      | J2GRID       | Current density                           |
  +---------------------------------+--------------+-------------------------------------------+

.. _mag-property:

Property
~~~~~~~~
::

  --- input block ---
  [KEYMAG] <opt>
  --- input block ---

  <opt>  -->  GT-4   -->  g-tensor
         -->  HFS-4  -->  Hyperfine coupling constants
                          (HFS: Hyperfine structure)
         -->  CS-4   -->  NMR shielding tensor
                          (CS: Chemical Shielding)
         -->  SR-4   -->  Nuclear spin-rotation coupling constants
         -->  JMN-4  -->  spin-spin coupling constants
                          (JMN: J-coupling between nuclei M and N)

::

  --- default ---
  KEYMAG CS-4
  --- default ---

::

  --- example ---
  KEYMAG GT-4
  --- example ---

.. _mag-substitute:

Specify properties of atoms
~~~~~~~~~~~~~~~~~~~~~~~~~~~
::

  --- input block ---
  [SUBSITUTE] <atom>
  [GVALUE]    <opt1> |or|
  [ISOTOPE]   <opt2> |or|
  [MASS]      <opt3> |or|
  [GRID]      <opt4>
  END
  --- input block ---

  Properties of atoms (g-value, ...) will be assigned to atom(s) specified with option <atom>
  <atom>  -->  atom number     -->  number of the atom as specified in the geometry of the molecule
          -->  atom label      -->  atom label (f.e. He)

  g-value:
  <opt1>   -->  real number: g-value of the atom

  Isotope:
  <opt2>   -->  positive integer number: isotope of the atom (g-value and mass of this isotope will be set automatically)

  Mass:
  <opt3>   -->  real number: mass of the atom

  Grid:
  It is possible to specify grid as separate keyword. This will set the same grid for all atoms in the molecule.
  <opt4>   -->  <radial> |and| <angular>
                -->  <radial>   -->  integer number: radial number of points
                -->  <angular>  -->  integer number: angular number of points
                                -->  only following values are allowed:
                                     110, 146, 194, 302, 434, 590, 770, 974, 1202
           -->  <radial> ADAPTIVE
                -->  recommended option
                -->  <radial>   -->  integer number: radial number of points
                -->  first period (H, He) should have at least 50 radial points
                     for every other period add at least 10 grid points


::

  --- default ---
  Grid from .11 file and properties for isotope with maximum abundance will be used.
  --- default ---

::

  --- example 1 ---
  SUBSTITUTE N
  ISOTOPE 15
  GRID 60 ADAPTIVE
  END
  --- example 1 ---

::

  --- example 2 ---
  SUBSTITUTE 5
  MASS  15.0001088982
  GRID  70  194
  END
  --- example 2 ---

.. _mag-reference-atom:

Reference atom
~~~~~~~~~~~~~~

This keyword is used in calculations of JMN.

::

  --- input block ---
  [REFATOM] <num>
  --- input block ---

  <num>  -->  positive integer number

::

  --- default ---
  REFATOM 1
  --- default ---

::

  --- example ---
  REFATOM 3
  --- example ---

.. _mag-nmr-atoms:

NMR atoms
~~~~~~~~~

This keyword is used in calculations of CS, SR and JMN.

::

  --- input block ---
  [NMRATOM] <opt1> |I| { <num> |and|
  <atom1> <atom2> ... }
  --- input block ---

  It will specify atoms for which corresponding property will be calculated.
  (J-coupling will be calculated between reference atom and these atoms)
  <opt1>  -->  ALL   -->  all atoms will be used
          -->  LIST  -->  user defined list of atoms
                     -->  <num>   -->  positive integer number
                                  -->  the number of atoms in the list
                          <atomx> -->  positive integer number
                                  -->  the number of the atom in the molecule (see .inp file)

::

  --- default ---
  NMRATOM ALL
  --- default ---

::

  --- example ---
  NMRATOM LIST 3
  1 3 4
  --- example ---

.. _mag-common-guage-origin:

Common guage origin
~~~~~~~~~~~~~~~~~~~

This keyword is used in calculations of g-tensor or CS-CGO.

::

  --- input block ---
  [COMMONGAUGE] <opt1> |I| { <opt2> |I|
  <x> <y> <z> }
  --- input block ---

  <opt1>  -->  ATOM   -->  put gauge origin on some atom
                      -->  <opt2>  -->  positive integer: number of the atom
          -->  ZERO   -->  put gauge in the origin of the coordinate system
          -->  POINT  -->  manually specify gauge origin
                      -->  <x>  -->  x coordinate
                      -->  <y>  -->  y coordinate
                      -->  <z>  -->  z coordinate

::

  --- default ---
  COMMONGAUGE ZERO
  --- default ---

::

  --- example 1 ---
  COMMONGAUGE ATOM 2
  --- example 1 ---

::

  --- example 2 ---
  COMMONGAUGE POINT
  -1.034 1.0E-3 3.5D+01
  --- example 2 ---

.. _mag-level-of-theory:

Level of theory
~~~~~~~~~~~~~~~

This keyword is used in calculations of CS, SR and JMN.

::

  --- input block ---
  [PERTURBATION] <opt>
  --- input block ---

  <opt>  --> UNCOUPLED -->  uncoupled equations
                       -->  it is computationally efficient but it is only
                            an approximation, do not use it in JMN calculations
                            and use it with caution in CS and SR calculations
         --> COUPLED   -->  caupled equations
                       -->  this as a most plausible option

::

  --- default ---
  PERTURBATION COUPLED
  --- default ---

::

  --- example ---
  PERTURBATION UNCOUPLED
  --- example ---

.. _mag-operator:

Operator
~~~~~~~~
::

  --- input block ---
  [OPERATOR] <opt>
  --- input block ---

  Four-component matrix Dirac-Kohn-Sham theory:
  <opt>  -->  mDKS        -->  without fitting
         -->  mDKS-RI-J   -->  only coulomb potential is fitted
         -->  mDKS-RI-JK  -->  both coulomb and exchange-correlation potential are fitted

::

  --- default ---
  OPERATOR mDKS
  --- default ---

::

  --- example ---
  OPERATOR mDKS-RI-J
  --- example ---

.. _mag-gauge-origin-problem:

Gauge origin problem
~~~~~~~~~~~~~~~~~~~~

This keyword is used in calculations of CS.

::

  --- input block ---
  [METHOD] <opt>
  --- input block ---

  <opt>  -->  CGO   -->  common gauge origin
                    -->  use COMMONGAUGE keyword for specifying gauge origin
         -->  GIAO  -->  gauge including atomic orbitals

::

  --- default ---
  METHOD GIAO
  --- default ---

::

  --- example ---
  METHOD CGO
  --- example ---

.. _mag-kernel:

Method for calculation of kernel
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This keyword is used in calculations of CS or JMN.

::

  --- input block ---
  [KERNEL] <opt1> |I| <opt2>
  --- input block ---

  <opt1>  -->  NUMERICAL  -->  Kernel will be calculated numerically

  Parameter for numerical derivation:
  <opt2>  -->  positive real number

::

  --- default ---
  KERNEL NUMERICAL 1.0D-3
  --- default ---

::

  --- example ---
  KERNEL NUMERICAL
  --- example ---

.. _mag-type-of-kernel:

Type of kernel
~~~~~~~~~~~~~~

This keyword is used in calculations of CS or JMN.

::

  --- input block ---
  [POTENTIAL] <opt>
  --- input block ---

  Potential from which will be constructed kernel:
  <opt>  -->  LOCAL   -->  [2]     Slater exchange / VWN correlation
                      -->          in literature: VWN5
         -->  BP86    -->  [3]     Becke exchange / Perdew correlation
         -->  PBE     -->  [4]
         -->  SCF     -->  use the same potential as was used in
                           unperturbed SCF calculation

::

  --- default ---
  POTENTIAL SCF
  --- default ---

::

  --- example ---
  POTENTIAL LOCAL
  --- example ---

.. _mag-diis:

Direct Inversion in the Iterative Space
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It is used in COUPLED calculations of CS, SR or JMN. The options are the same as in basic SCF input.

::

  --- input block ---
  [DIIS] <opt1> |I| { <opt2> |and| <opt3> |and| <opt4> }
  --- input block ---

  <opt1>  -->  OFF  -->  diis scheme is turned off
          -->  ON   -->  diis scheme is turned on

  Set parameters for diis:
  <opt2>, <opt3>, <opt4>  -->  positive integer numbers
  <opt2>  -->  The leading fock matrix to diis interpolation
          -->  Must be greater then one
  <opt3>  -->  Number of fock matrices from which the diis interpolation
               will start
          -->  It must be less then <opt4>
  <opt4>  -->  Maximum number of fock matrices used in diis
               interpolation

::

  --- default ---
  DIIS ON 2 4 49
  --- default ---

::

  --- example ---
  DIIS OFF
  --- example ---

.. _mag-mixing-parameter:

Mixing parameter
~~~~~~~~~~~~~~~~

This keyword is used in COUPLED calculations of CS, SR or JMN.

::

  --- input block ---
  [DMIXING] <num>
  --- input block ---

  <num>  -->  positive real number

::

  --- default ---
  DMIXING 0.2D0
  --- default ---

::

  --- example ---
  DMIXING 0.1
  --- example ---

.. _mag-maximum-iterations:

Maximum number of SCF iterations
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This keyword is used in COUPLED calculations of CS, SR or JMN.

::

  --- input block ---
  [MAXITER] <opt>
  --- input block ---

  <opt>  -->  positive integer number  -->  maximum number of iterations

::

  --- default ---
  MAXITER 20
  --- default ---

::

  --- example ---
  MAXITER 10
  --- example ---

.. _mag-convergent-criteria:

Convergence criteria
~~~~~~~~~~~~~~~~~~~~

This keyword is used in COUPLED calculations of CS, SR or JMN.

::

  --- input block ---
  [CONVERGENCE] <opt1> |I| <opt2>
  --- input block ---

  <opt1>  -->  positive real number
          -->  maximum number of DIIS error vector
  <opt2>  -->  positive real number
          -->  maximum change in beta coefficients between iterations
          -->  if not specified it will be used the <opt1> number

::

  --- default ---
  CONVERGENCE  1.0D-07 1.0D-06
  --- default ---

::

  --- example ---
  CONVERGENCE 1.0D-06
  --- example ---

.. _mag-cscale:

Speed of the light
~~~~~~~~~~~~~~~~~~
::

  --- input block ---
  [CSCALE] <opt>
  --- input block ---

  <opt>  -->  positive real number  -->  clight = clight * <opt>
         -->  recommended number equivalent to infinity is 50

::

  --- default ---
  Speed of light is not changed
  --- default ---

::

  --- example ---
  CSCALE 10
  --- example ---

.. _mag-output-file:

Form of the output file
~~~~~~~~~~~~~~~~~~~~~~~
::

  --- input block ---
  [OUTPUT] <opt>
  --- input block ---

  <opt>  -->  NORMAL  -->  Normal output
         -->  LONG    -->  Long output
         -->  DEBUG   -->  New file DEBUG will be created with debuging info
         -->  LDEBUG  -->  New file DEBUG will be created with extended debuging info

::

  --- default ---
  OUTPUT NORMAL
  --- default ---

::

  --- example ---
  OUTPUT DEBUG
  --- example ---

.. _mag-charge-distribution:

Charge distribution of nucleus
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
::

  --- input block ---
  [NUCLEUS] <opt>
  --- input block ---

  <opt>  -->  POINT  -->  Point charge distribution of nucleus
         -->  GAUSS  -->  Gauss charge distribution of nucleus
         -->  SCF    -->  use the same model as in unperturbed SCF

::

  --- default ---
  NUCLEUS POINT
  --- default ---

::

  --- example ---
  NUCLEUS GAUSS
  --- example ---

.. _mag-magnetic-moment:

Type of magnetic moment of nucleus
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
::

  --- input block ---
  [MAGMOMENT] <opt>
  --- input block ---

  <opt>  -->  POINT  -->  Point magnetic moment of nucleus
         -->  GAUSS  -->  Gauss magnetic moment of nucleus

::

  --- default ---
  MAGMOMENT POINT
  --- default ---

::

  --- example ---
  MAGMOMENT GAUSS
  --- example ---

.. _mag-density2grid:

Calculate density on the grid
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
::

  --- input block ---
  [R2GRID] <opt1> ...
  ...
  --- input block ---

  <opt1>  -->  NORMAL  -->  Density will be calculated on standart grid used
                            in SCF calculation or specified using GRID keyword
          -->  1DIM    -->  Density will be calculated on one dimensional line
          -->  CUBE    -->  The cube file in gaussian format will be created

::

  --- default ---
  Do not print density on grid.
  --- default ---

::

  --- example 1 ---
  R2GRID CUBE
  --- example 1 ---

-------------------------  1DIM  -------------------------

::

  --- input block ---
  [R2GRID] [1DIM] [ATOM] |I| [LOG]
  <ngrid> <dist>
  <atom1> <atom2>
  --- input block ---

  ATOM  -->  The line will be defined using atoms
  LOG   -->  The output densities will be in logarithmic scale

  Number of grid points:
  <ngrid> -->  positive integer number

  Distance from <atom1> where the grid poits will end:
  <dist>  -->  positive real number

  Starting point (atom):
  <atom1> -->  positive integer number (number of the atom from .inp file)

  Middle atom; it determines the direction:
  <atom2> -->  positive integer number (number of the atom from .inp file)

::

  --- example 2 ---
  R2GRID 1DIM ATOM
  1000 3.5d0
  3 1
  --- example 2 ---

::

  --- input block ---
  [R2GRID] [1DIM] |I| [LOG]
  <ngrid> <dist>
  <p1x> <p1y> <p1z>
  <p2x> <p2y> <p2z>
  --- input block ---

  Coordinates of the starting point:
  <p1x>, <p1y>, <p1z>  -->  real numbers

  Coordinates of the middle point:
  <p2x>, <p2y>, <p2z>  -->  real numbers

-------------------------  CUBE  -------------------------

::

  --- input block ---
  [R2GRID] [CUBE] |and| <opt>
  <oriqx> <origy> <origz>
  <nx> <ny> <nz>
  <stepx> <stepy> <stepz> |I|
  <x1> <y1> <z1>
  <x2> <y2> <z2>
  --- input block ---

  <opt>  -->  RHO0  -->  Electronic density
         -->  RHOX  -->  Spin x density
         -->  RHOY  -->  Spin y density
         -->  RHOZ  -->  Spin z density

  Starting point of constructing the cube file:
  <oriqx>, <origy>, <origz>  -->  real numbers

  Number of grid points in x, y and z directions:
  <nx>, <ny>, <nz>  -->  positive integer numbers

  Steps between the grid points in x, y and z directions:
  <stepx>, <stepy>, <stepz>  -->  positive real numbers

  Directions in which should be performed steps:
  <x1>, <y1>, <z1>  -->  real numbers (first vector)
  <x2>, <y2>, <z2>  -->  real numbers (second vector)

::

  --- example 3 ---
  R2GRID CUBE RHO0
  -6.0d0 -6.0d0 -10.0d0
  60 60 200
  0.20d0 0.20d0 0.12d0
  1.0d0 0.0d0 0.0d0
  0.0d0 1.0d0 0.0d0
  --- example 3 ---

::

  --- default ---
  Do not print density to output
  --- default ---

.. _mag-keys-for-R2GR:

Options for R2GRID keyword
~~~~~~~~~~~~~~~~~~~~~~~~~~
::

  --- input block ---
  [KEYS] <opt1> |or| <opt2>
  --- input block ---

  <opt1>  -->  BOTH  -->  both densities will be written on output
          -->  FIT   -->  fitted density will be written on output
          -->  EXACT -->  exact density will be written on output

  <opt2>  -->  ANGSTROM  -->  input values are in angstroms
          -->  BOHR      -->  input values are in bohrs

::

  --- default ---
  KEYS BOTH BOHR
  --- default ---

::

  --- example ---
  KEYS EXACT ANGSTROMS
  --- example ---

.. _mag-current-density:

Current density
~~~~~~~~~~~~~~~

Current density is available only for CS calculations.

::

  --- input block ---
  [J2GRID] <opt1> |or| <opt2> |or| <opt3> |or| <opt4>
  --- input block ---

  <opt1>  -->  PLANE    -->  The current will be calculated on the plane
          -->  CUBE     -->  The current will be calculated in the cube

  <opt2>  -->  BOHR     -->  Coordinates on input are in atomic units (Bohrs)
          -->  ANGSTROM -->  Coordinates on input are in angstroms
          -->           -->  Default: BOHR

  <opt3>  -->  STOP     -->  STOP calculation after geting the current
          -->           -->  Default: the calculation of NMR shielding will be carried out

  <opt4>  -->  XONLY    -->  Use only magnetic field along X axes
          -->  YONLY    -->  Use only magnetic field along Y axes
          -->  ZONLY    -->  Use only magnetic field along Z axes
          -->           -->  Without specification all orientations of magnetic field will be calculated

::

  --- default ---
  Do not calculate current density.
  --- default ---

::

  --- example 1 ---
  J2GRID PLANE YONLY STOP BOHR
  --- example 1 ---

------------------------  PLANE  -----------------------

::

  --- input block ---
  [J2GRID] [PLANE] ...
  <oriqx> <origy> <origz>
  <x1> <x2> <x3>
  <y1> <y2> <y3>
  <nx> <ny>
  --- input block ---

  Starting point of constructing the two dimensional plane:
  <oriqx>, <origy>, <origz>  -->  real numbers

  Vectors defining edges of the plane (they starts in the origin):
  <x1>, <x2>, <x3>  -->  real numbers (X edge)
  <y1>, <y2>, <y3>  -->  real numbers (Y edge)

  Number of grid points on x and y edge:
  <nx>, <ny>  -->  positive integer numbers

::

  --- example 2 ---
  J2GRID PLANE ZONLY STOP
   -6.0d0 -6.0d0  0.0d0
   12.0d0  0.0d0  0.0d0
    0.0d0 12.0d0  0.0d0
  100 100
  --- example 2 ---

------------------------  CUBE  -----------------------

::

  --- input block ---
  [J2GRID] [CUBE] ...
  <oriqx> <origy> <origz>
  <x1> <x2> <x3>
  <y1> <y2> <y3>
  <zlength>
  <nx> <ny> <nz>
  --- input block ---

  Starting point of constructing the three dimensional cube:
  <oriqx>, <origy>, <origz>  -->  real numbers

  Vectors defining edges of the cube (they starts in the origin) and length of Z edge:
  <x1>, <x2>, <x3>  -->  real numbers (X edge)
  <y1>, <y2>, <y3>  -->  real numbers (Y edge)
  <zlength>         -->  real number  (length of Z edge)
                    -->  Z edge will be perpendicular to X and Y edge

  Number of grid points on x, y and z edge:
  <nx>, <ny>, <nz>  -->  positive integer numbers

::

  --- example 3 ---
  J2GRID CUBE ZONLY
   -6.0d0 -6.0d0  0.0d0
   12.0d0  0.0d0  0.0d0
    0.0d0 12.0d0  0.0d0
    6.0d0
  100 100 100
  --- example 3 ---
