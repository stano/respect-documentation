Calculation of the nuclear spin-rotation tensor for closed-shell systems
========================================================================

Procedure for calculation of nuclear spin-rotation tensor is essentially the
same as for NMR shielding tensor described above. The only difference is in
three lines in the input file.

**“Gold standard” of the input file for the nuclear spin-rotation calculation:**
::

  KEYMAG         SR-4
  METHOD         CGO
  PERTURBATION   COUPLED
  OPERATOR       mDKS
  #
  KERNEL         ANALYTICAL
  DMIXING        1.0D0
  DIIS           ON 1 3 49
  MAXITER        30
  CONVERGENCE    1.0D-5

First four lines define method:


* four-component nuclear spin-rotation calculation
* Common Gauge Origin method (by default it is set to the centre of nuclear mass)
* coupled perturbed scheme
* four-component Dirac-Kohn-Sham Hamiltonian (no fitting is employed)


Example
-------

The attachment (:download:`ReSpect_example_SR
<../files/ReSpect_3.3.0_example_NMR-CS-GIAO.tar.gz>`) contains an example of the
nuclear spin-rotation calculation of the HI molecule. First update path to your
ReSpect install directory and scratch directory in two simple bash
shell-scripts (*run-1-scf* and *run-2-sr*). Then run these scripts sequentially:

::

  $ run-1-scf
  $ run-2-nmr

The “ref” directory contains reference data.
