Running SCF: optimization of ground state MO coefficients
=========================================================

A ground state optimization of Dirac-Hartree-Fock (DHF) or Dirac-Kohn-Sham
(DKS) energy requires typically 32x more variational parameters than the
conventional non-relativistic case. It is therefore desirable to have a good
initial guess of the ground state molecular orbitals (MO). On an example of HI
molecule, we demonstrate a typical three-steps approach for converging
relativistic SCF procedure, as recommended by ReSpect authors. For clarity,
reference input/output files associated with the steps are also attached. 

1. perform the SCF optimization with a **scalar one-electron DKH2 Hamiltonian**
   and an **atomic initial guess** for molecular orbitals (SCF keyword “INITMO
   ATOMIC”). The reference input/output files are attached
   (:download:`ReSpect_example_SCF <../files/ReSpect_3.3.0_example_SCF.tar.gz>`).
2. prepare a **restart file** to next four-component calculations; make link/copy
   of previous_scf_job.50 to next_scf_job.rest.
3. perform the SCF optimization with a final **four-component Hamiltonian** and a
   **restart initial guess** for molecular orbitals (SCF keyword “INITMO RESTART”).
   The reference input/output files are attached
   (:download:`ReSpect_example_SCF <../files/ReSpect_3.3.0_example_SCF.tar.gz>`).
   Note that the restart from one-component calculations is not limited to the use
   of the same DFT functional and/or basis sets.


How to run the SCF jobs
-----------------------
::

  $ /path/to/ReSpect/RUN_RS-mDKS -scratch /path/to/scratch scf_job

* *scf_job*: name of the SCF input file (without .inp extension)
* for more details, see the manual :ref:`manual <scf-manual>`

**Final remark**: SCF optimizations finish successfully when the variation in
electronic energy (dE) as well as DIIS error vector ([F,D]) underlays an input
threshold value (SCF keyword “CONVERGENCE“). In this case, “*Happy end*” message
pops up in the output.
