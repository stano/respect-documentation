Calculation of the NMR shielding tensor for closed-shell systems
================================================================

Calculation of the NMR shielding tensor for closed-shell systems is a two steps
approach:

1. Calculation of the **perturbation-free MO coefficients** using SCF module
2. Calculation of the **NMR shielding tensor** using MAG module

Important: Before proceeding to step 2 make sure that step 1 finished
successfully (“Happy end” will appear at the end of the output file).

Calculation of the NMR shielding tensor
---------------------------------------

To calculate NMR shielding tensor run the following command
::

  $ /path/to/ReSpect/RUN_MAG-mDKS_NMR -scratch /path/to/scratch scf_job mag_input


* *scf_job*: name of the SCF input file (without .inp extension)
* *mag_input*: name of the input file for MAG module (mag_input.M)
* for more details, see :ref:`manual <mag-manual>`


**“Gold standard” of the input file for the NMR shielding tensor calculation:**
::

  KEYMAG         CS-4
  METHOD         GIAO
  PERTURBATION   COUPLED
  OPERATOR       mDKS-RI-J
  #
  KERNEL         ANALYTICAL
  DMIXING        1.0D0
  DIIS           ON 1 3 49
  MAXITER        30
  CONVERGENCE    1.0D-5

First four lines define method for the NMR shielding calculation:

* four-component chemical shielding calculation
* Gauge-Including-Atomic-Orbitals
* coupled perturbed scheme
* fitting of the coulomb potential

Last five lines define details of the self consistent (coupled) scheme.

Note: detailed description of all keywords is given in the :ref:`manual <mag-manual>`
for the MAG module.

Results
-------

Results of the calculation can be found in the scf_job-mag_input.OUT file.

Example
-------

The attachment (:download:`ReSpect_example_NMR-CS-GIAO
<../files/ReSpect_3.3.0_example_NMR-CS-GIAO.tar.gz>`) contains an example of
the NMR shielding tensor calculation of the HI molecule.  First update path to
your ReSpect install directory and scratch directory in two simple bash
shell-scripts (*run-1-scf* and *run-2-nmr*). Then run these scripts
sequentially:

::

  $ run-1-scf
  $ run-2-nmr

The “ref” directory contains reference data.
