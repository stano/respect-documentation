Welcome to ReSpect's documentation!
===================================

There are in principle two parts of ReSpect:

* SCF module – calculation of unperturbed ground state MO coefficients

* MAG module – calculation of the magnetic properties

Each part requires a specific input file (.inp and .M). For description
of keywords check the reference manual.

List of features:

* EPR
    * g-tensor
    * hyperfine coupling tensor

* NMR
    * NMR shielding tensor
    * nuclear spin-rotation coupling tensor
    * indirect nuclear spin-spin coupling tensor

Manual
^^^^^^

.. toctree::
   :maxdepth: 2

   manual/scf
   manual/mag

Tutorial
^^^^^^^^

.. toctree::
   :maxdepth: 2

   tutorial/running_scf
   tutorial/CS_closed-shell
   tutorial/SR_closed-shell
